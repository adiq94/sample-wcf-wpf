# README #

### Description ###

Used technologies:

* WCF hosted in console application
* WPF + MVVM Light
* Microsofty Unity (DI), Moq, xUnit
* Entity Framework Code First + Command–query separation pattern
* Unity Interception (AOP)

### Quickstart ###
* Download project
* Set multiple startup projects, WCFConsoleHost and WPFApplication1
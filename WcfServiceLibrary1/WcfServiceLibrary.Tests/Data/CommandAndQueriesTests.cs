﻿using Moq;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using WcfServiceLibrary.Data.Database;
using WcfServiceLibrary.Data.Database.Commands;
using WcfServiceLibrary.Data.Database.Queries;
using WcfServiceLibrary.Data.Model;
using Xunit;

namespace WcfServiceLibrary.Tests.Data
{
    public class CommandAndQueriesTests
    {
        [Fact]
        public void AddSampleTest()
        {
            var mockSet = new Mock<DbSet<Sample>>();

            var mockContext = new Mock<SampleContext>();
            mockContext.Setup(x => x.Samples).Returns(mockSet.Object);

            AddSampleCommandHandler commandHandler = new AddSampleCommandHandler(mockContext.Object);
            commandHandler.Execute(new AddSampleCommand("testAddCommand"));

            mockSet.Verify(x => x.Add(It.IsAny<Sample>()), Times.Once);
            mockContext.Verify(x => x.SaveChanges(), Times.Once);
        }

        [Fact]
        public void DeleteSampleTest()
        {
            var mockSet = new Mock<DbSet<Sample>>();

            var mockContext = new Mock<SampleContext>();
            mockContext.Setup(x => x.Samples).Returns(mockSet.Object);
            mockContext.Setup(x => x.SetDeleted(It.IsAny<object>()));

            DeleteSampleCommandHandler commandHandler = new DeleteSampleCommandHandler(mockContext.Object);
            commandHandler.Execute(new DeleteSampleCommand(1));

            mockSet.Verify(x => x.Attach(It.IsAny<Sample>()), Times.Once);
            mockContext.Verify(x => x.SetDeleted(It.IsAny<Sample>()), Times.Once);
            mockContext.Verify(x => x.SaveChanges(), Times.Once);
        }

        [Fact]
        public void EditSampleTest()
        {
            var mockSet = new Mock<DbSet<Sample>>();

            var mockContext = new Mock<SampleContext>();
            mockContext.Setup(x => x.Samples).Returns(mockSet.Object);
            mockContext.Setup(x => x.SetModified(It.IsAny<object>()));

            EditSampleCommandHandler commandHandler = new EditSampleCommandHandler(mockContext.Object);
            commandHandler.Execute(new EditSampleCommand(new Sample() { SampleId = 1, Name = "test" }));

            mockSet.Verify(x => x.Attach(It.IsAny<Sample>()), Times.Once);
            mockContext.Verify(x => x.SetModified(It.IsAny<Sample>()), Times.Once);
            mockContext.Verify(x => x.SaveChanges(), Times.Once);
        }

        [Fact]
        public void GetSamplesTest()
        {
            var samplesMock = new List<Sample>
            {
                new Sample {SampleId = 1, Name = "test" },
                new Sample {SampleId = 2, Name = "test2" }
            }.AsQueryable();
            var mockContext = SetupQueryMock(samplesMock);

            SampleQuery query = new SampleQuery(mockContext);
            IEnumerable<Sample> result = query.Execute();
            Assert.Equal(samplesMock.AsEnumerable(), result);
        }

        private SampleContext SetupQueryMock(IQueryable<Sample> samples)
        {
            var mockSet = new Mock<DbSet<Sample>>();
            mockSet.As<IQueryable<Sample>>().Setup(m => m.Provider).Returns(samples.Provider);
            mockSet.As<IQueryable<Sample>>().Setup(m => m.Expression).Returns(samples.Expression);
            mockSet.As<IQueryable<Sample>>().Setup(m => m.ElementType).Returns(samples.ElementType);
            mockSet.As<IQueryable<Sample>>().Setup(m => m.GetEnumerator()).Returns(samples.GetEnumerator());

            var mockContext = new Mock<SampleContext>();
            mockContext.Setup(x => x.Samples).Returns(mockSet.Object);
            return mockContext.Object;
        }
    }
}
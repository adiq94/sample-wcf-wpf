﻿using Microsoft.Practices.Unity.InterceptionExtension;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1.InterceptionBehaviors
{
    public class LoggingInterceptionBehavior : IInterceptionBehavior
    {
        private readonly ILogger _logger;
        public LoggingInterceptionBehavior(ILogger logger)
        {
            _logger = logger;
        }
        public bool WillExecute
        {
            get
            {
                return true;
            }
        }

        public IEnumerable<Type> GetRequiredInterfaces()
        {
            return Type.EmptyTypes;
        }

        public IMethodReturn Invoke(IMethodInvocation input, GetNextInterceptionBehaviorDelegate getNext)
        {
            var text = String.Format("{0}:Invoking method {1} with parameters: ", DateTime.Now, input.MethodBase.Name);
            foreach (var item in input.Arguments)
            {
                text += item;
                text += " ";
            }
            _logger.Log(text);

            var result = getNext()(input, getNext);

            if (result.Exception != null)
            {
                _logger.Log(String.Format("{0}: Method {1} threw exception: {2}", DateTime.Now, input.MethodBase.Name, result.Exception.Message));
            }
            else
            {
                _logger.Log(String.Format("{0}: Method {1} returned {2}", DateTime.Now, input.MethodBase.Name, result.ReturnValue ));
            }

            return result;
        }
    }
}

﻿using ConsoleApplication1.InterceptionBehaviors;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.InterceptionExtension;
using System;
using System.Data.Entity;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Description;
using WcfServiceLibrary.Data.Database;
using WcfServiceLibrary.Data.Database.Commands;
using WcfServiceLibrary.Data.Database.Core.Base;
using WcfServiceLibrary.Data.Database.Queries;
using WcfServiceLibrary1;
using WcfServiceLibrary1.InternalServices;

namespace ConsoleApplication1
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            using (IUnityContainer container = new UnityContainer())
            {
                RegisterTypes(container);

                Uri httpBaseAddress = new Uri("http://localhost:1234/SampleService");

                ServiceHost sampleHost = new UnityServiceHost(container, typeof(Service1), httpBaseAddress);
                try
                {
                    sampleHost.AddServiceEndpoint(typeof(IService1), new WSHttpBinding(), "");

                    ServiceMetadataBehavior serviceBehavior = new ServiceMetadataBehavior();
                    serviceBehavior.HttpGetEnabled = true;
                    sampleHost.Description.Behaviors.Add(serviceBehavior);

                    sampleHost.Open();
                    Console.WriteLine("Serwer nasłuchuje na: {0}", httpBaseAddress);
                    Console.ReadKey();
                    sampleHost.Close();
                }
                catch (Exception e)
                {
                    sampleHost.Abort();
                    Console.WriteLine("Wyjątek: " + e.Message);
                    Console.ReadKey();
                }
            }
        }

        private static void RegisterTypes(IUnityContainer container)
        {
            container.RegisterType<ILogger, Logger>();
            container.AddNewExtension<Interception>();

            container.RegisterType<Func<Type, Object>>(new InjectionFactory(x => new Func<Type, Object>((y) => x.Resolve(y))));
            container.RegisterType<Func<Type, Object[]>>(new InjectionFactory(x => new Func<Type, Object[]>((y) => x.ResolveAll(y).ToArray())));

            //Queries
            container.RegisterType<IQueryFactory, QueryFactory>();
            container.RegisterType<ISampleQuery, SampleQuery>();

            //Commands
            container.RegisterType<ICommandsFactory, CommandFactory>();
            container.RegisterType<ICommandHandler<AddSampleCommand>, AddSampleCommandHandler>("AddSampleCommand");
            container.RegisterType<ICommandHandler<EditSampleCommand>, EditSampleCommandHandler>("EditSampleCommand");
            container.RegisterType<ICommandHandler<DeleteSampleCommand>, DeleteSampleCommandHandler>("DeleteSampleCommand");

            container.RegisterType<ITestService, TestService>(
                new Interceptor<InterfaceInterceptor>(),
                new InterceptionBehavior<LoggingInterceptionBehavior>());
            container.RegisterType<IComplexTestService, ComplexTestService>(
                new Interceptor<InterfaceInterceptor>(),
                new InterceptionBehavior<LoggingInterceptionBehavior>());

            Database.SetInitializer(new DatabaseInitializer());
            SampleContext db = new SampleContext();
            db.Database.Initialize(true);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    public class Logger : ILogger
    {
        private bool toggle;
        public void Log(string message)
        {
            toggle = !toggle;
            if(toggle)
            {
                Console.ForegroundColor = ConsoleColor.Magenta;
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Green;
            }
            Console.WriteLine(message);
        }
    }

    public interface ILogger
    {
        void Log(string message);
    }
}

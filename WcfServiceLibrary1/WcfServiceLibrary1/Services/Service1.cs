﻿using System;
using System.Collections.Generic;
using System.Linq;
using WcfServiceLibrary.Data.Model;
using WcfServiceLibrary1.InternalServices;

namespace WcfServiceLibrary1
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in both code and config file together.
    public class Service1 : IService1
    {
        private readonly IComplexTestService _complexService;
        private readonly ITestService _testService;

        public Service1(ITestService testService, IComplexTestService complexService)
        {
            _testService = testService;
            _complexService = complexService;
        }

        public Sample AddSample(Sample sample)
        {
            try
            {
                var result = _complexService.AddSample(sample);
                return result;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }
        }

        public ResponseMessage DeleteSample(int sampleId)
        {
            try
            {
                _complexService.DeleteSample(sampleId);
                return new ResponseMessage();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return new ResponseMessage { ErrorCode = 1, ErrorMessage = "Wystąpił błąd" };
            }
        }

        public ResponseMessage EditSample(Sample sample)
        {
            try
            {
                _complexService.EditSample(sample);
                return new ResponseMessage();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return new ResponseMessage { ErrorCode = 1, ErrorMessage = "Wystąpił błąd" };
            }
        }

        public string GetData(int value)
        {
            return _testService.ReturnSampleString(value);
        }

        public List<Sample> GetSamples()
        {
            try
            {
                var result = _complexService.GetSamples().ToList();
                return result;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }
        }
    }
}
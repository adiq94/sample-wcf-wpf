﻿using System.Collections.Generic;
using System.ServiceModel;
using WcfServiceLibrary.Data.Model;

namespace WcfServiceLibrary1
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IService1
    {
        [OperationContract]
        Sample AddSample(Sample sample);

        [OperationContract]
        ResponseMessage DeleteSample(int sampleId);

        [OperationContract]
        ResponseMessage EditSample(Sample sample);

        [OperationContract]
        string GetData(int value);

        [OperationContract]
        List<Sample> GetSamples();
    }
}
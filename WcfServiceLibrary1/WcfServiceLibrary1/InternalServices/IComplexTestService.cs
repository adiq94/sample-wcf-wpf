﻿using System.Collections.Generic;
using WcfServiceLibrary.Data.Model;

namespace WcfServiceLibrary1.InternalServices
{
    public interface IComplexTestService
    {
        Sample AddSample(Sample sample);

        void DeleteSample(int sampleId);

        void EditSample(Sample sample);

        IEnumerable<Sample> GetSamples();
    }
}
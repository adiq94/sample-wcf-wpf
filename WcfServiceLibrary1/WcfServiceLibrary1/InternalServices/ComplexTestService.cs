﻿using System.Collections.Generic;
using WcfServiceLibrary.Data.Database.Commands;
using WcfServiceLibrary.Data.Database.Core.Base;
using WcfServiceLibrary.Data.Database.Queries;
using WcfServiceLibrary.Data.Model;

namespace WcfServiceLibrary1.InternalServices
{
    public class ComplexTestService : IComplexTestService
    {
        private readonly ICommandsFactory _commandsFactory;
        private readonly IQueryFactory _queryFactory;

        public ComplexTestService(IQueryFactory queryFactory, ICommandsFactory commandsFactory)
        {
            _queryFactory = queryFactory;
            _commandsFactory = commandsFactory;
        }

        public Sample AddSample(Sample sample)
        {
            AddSampleCommand addSampleCommand = new AddSampleCommand(sample.Name);
            _commandsFactory.ExecuteQuery(addSampleCommand);
            sample.SampleId = addSampleCommand.SampleId;
            return sample;
        }

        public void DeleteSample(int sampleId)
        {
            DeleteSampleCommand deleteSampleCommand = new DeleteSampleCommand(sampleId);
            _commandsFactory.ExecuteQuery(deleteSampleCommand);
        }

        public void EditSample(Sample sample)
        {
            EditSampleCommand editSampleCommand = new EditSampleCommand(sample);
            _commandsFactory.ExecuteQuery(editSampleCommand);
        }

        public IEnumerable<Sample> GetSamples()
        {
            var query = _queryFactory.ResolveQuery<ISampleQuery>();
            return query.Execute();
        }
    }
}
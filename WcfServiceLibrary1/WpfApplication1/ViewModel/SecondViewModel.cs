﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using WpfApplication1.Messaging;
using WpfApplication1.ServiceReference1;
using WpfApplication1.Services;
using WpfApplication1.View;

namespace WpfApplication1.ViewModel
{
    public class SecondViewModel : ViewModelBase
    {
        private readonly ITestService _testService;
        private bool _canLoadData;

        private Sample _currentSample;

        private FrameworkElement _currentView;

        private bool _dataLoaded;

        private int _selectedIndex;

        public SecondViewModel(ITestService testService)
        {
            BindCommands();
            _testService = testService;

            Samples = new ObservableCollection<Sample>();
            canLoadData = true;
            SelectedIndex = -1;
            SwitchView("");
        }

        public RelayCommand AddItemCommand { get; private set; }

        public ICommand ChangeToFirstViewCommand
        {
            get
            {
                return new RelayCommand(() =>
                {
                    Messenger.Default.Send<SwitchViewMessage>(new SwitchViewMessage { ViewName = "FirstView" });
                });
            }
        }

        public Sample CurrentSample
        {
            get
            {
                return _currentSample;
            }
            set
            {
                _currentSample = value;
                RaisePropertyChanged("CurrentSample");
            }
        }

        public FrameworkElement CurrentView
        {
            get { return _currentView; }
            set
            {
                _currentView = value;
                RaisePropertyChanged("CurrentView");
            }
        }

        public RelayCommand DeleteItemCommand { get; private set; }

        public RelayCommand DownloadDataCommand { get; private set; }

        public RelayCommand EditItemCommand { get; private set; }

        public RelayCommand GoToIndexCommand { get; private set; }

        public ObservableCollection<Sample> Samples { get; set; }

        public RelayCommand SaveCommand { get; private set; }

        public int SelectedIndex
        {
            get
            {
                return _selectedIndex;
            }
            set
            {
                _selectedIndex = value;
                EditItemCommand.RaiseCanExecuteChanged();
                DeleteItemCommand.RaiseCanExecuteChanged();
            }
        }

        private bool canLoadData
        {
            get
            {
                return _canLoadData;
            }
            set
            {
                _canLoadData = value;
                DownloadDataCommand.RaiseCanExecuteChanged();
            }
        }

        private bool dataLoaded
        {
            get
            {
                return _dataLoaded;
            }
            set
            {
                _dataLoaded = value;
                AddItemCommand.RaiseCanExecuteChanged();
            }
        }

        public async Task AddItem()
        {
            CurrentSample = new Sample();
            SwitchView("AddEdit");
        }

        public async Task DeleteItem()
        {
            var id = Samples[SelectedIndex].SampleId;
            var index = SelectedIndex;
            var result = await _testService.DeleteSample(id);
            if (result.ErrorCode == 0)
            {
                Samples.RemoveAt(index);
            }
        }

        public async Task EditItem()
        {
            CurrentSample = Samples[SelectedIndex];
            SwitchView("AddEdit");
        }

        public async Task LoadData()
        {
            try
            {
                canLoadData = false;
                IEnumerable<Sample> samples = await _testService.GetSamples();
                foreach (var item in samples)
                {
                    Samples.Add(item);
                }
                dataLoaded = true;
            }
            catch (Exception e)
            {
                canLoadData = true;
            }
        }

        public async Task Save()
        {
            SwitchView("Index");
            try
            {
                if (CurrentSample.SampleId == 0)
                {
                    var sample = await _testService.SaveSample(CurrentSample);
                    Samples.Add(sample);
                }
                else
                {
                    var result = await _testService.EditSample(CurrentSample);
                    if (result.ErrorCode == 0)
                    {
                        var sample = Samples.First(x => x.SampleId == CurrentSample.SampleId);
                        sample = CurrentSample;
                    }
                }
            }
            catch (Exception e)
            {
            }
        }

        public void SwitchView(string viewName)
        {
            switch (viewName)
            {
                case "Index":
                    CurrentView = new SecondViewIndex();
                    CurrentView.DataContext = this;
                    break;

                case "AddEdit":
                    CurrentView = new SecondViewAddEdit();
                    CurrentView.DataContext = this;
                    break;

                default:
                    CurrentView = new SecondViewIndex();
                    CurrentView.DataContext = this;
                    break;
            }
        }

        private void BindCommands()
        {
            DownloadDataCommand = new RelayCommand(() => LoadData(), () => canLoadData);
            AddItemCommand = new RelayCommand(() => AddItem(), () => dataLoaded);
            EditItemCommand = new RelayCommand(() => EditItem(), IsItemSelected);
            DeleteItemCommand = new RelayCommand(() => DeleteItem(), IsItemSelected);
            GoToIndexCommand = new RelayCommand(() => SwitchView("Index"));
            SaveCommand = new RelayCommand(() => Save());
        }

        private bool IsItemSelected()
        {
            if (SelectedIndex != -1)
            {
                return true;
            }
            return false;
        }
    }
}
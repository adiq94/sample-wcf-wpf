﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Threading.Tasks;
using System.Windows.Input;
using WpfApplication1.Messaging;
using WpfApplication1.Services;

namespace WpfApplication1.ViewModel
{
    public class FirstViewModel : ViewModelBase
    {
        private readonly ITestService _service;
        private bool _canLoadData;
        private string someText;

        public FirstViewModel()
        {
            _service = new TestService();
            SomeText = "tekst";
            DownloadTextCommand = new RelayCommand(() => LoadData(), () => canLoadData);
            canLoadData = true;
        }

        public ICommand ChangeToSecondViewCommand
        {
            get
            {
                return new RelayCommand(() =>
                {
                    Messenger.Default.Send<SwitchViewMessage>(new SwitchViewMessage { ViewName = "SecondView" });
                });
            }
        }

        public RelayCommand DownloadTextCommand { get; private set; }

        public string SomeText
        {
            get
            {
                return someText;
            }
            set
            {
                if (someText == value)
                {
                    return;
                }
                someText = value;
                RaisePropertyChanged("SomeText");
            }
        }

        private bool canLoadData
        {
            get
            {
                return _canLoadData;
            }
            set
            {
                _canLoadData = value;
                DownloadTextCommand.RaiseCanExecuteChanged();
            }
        }

        public bool CanLoadData()
        {
            return canLoadData;
        }

        public async Task LoadData()
        {
            try
            {
                canLoadData = false;
                SomeText = await _service.GetData(1);
            }
            catch (Exception e)
            {
                SomeText = e.Message;
            }
            finally
            {
                canLoadData = true;
            }
        }
    }
}
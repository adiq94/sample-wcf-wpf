using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Microsoft.Practices.ServiceLocation;
using System.Windows;
using System.Windows.Input;
using WpfApplication1.Messaging;
using WpfApplication1.View;

namespace WpfApplication1.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        private FrameworkElement _contentControlView;

        public MainViewModel()
        {
            Messenger.Default.Register<SwitchViewMessage>(this, (switchViewMessage) =>
            {
                SwitchView(switchViewMessage.ViewName);
            });
            SwitchView("");
        }

        public ICommand ChangeFirstViewCommand
        {
            get
            {
                return new RelayCommand(() =>
                {
                    SwitchView("FirstView");
                });
            }
        }

        public ICommand ChangeSecondViewCommand
        {
            get
            {
                return new RelayCommand(() =>
                {
                    SwitchView("SecondView");
                });
            }
        }

        public FrameworkElement ContentControlView
        {
            get { return _contentControlView; }
            set
            {
                _contentControlView = value;
                RaisePropertyChanged("ContentControlView");
            }
        }

        public void SwitchView(string viewName)
        {
            switch (viewName)
            {
                case "FirstView":
                    ContentControlView = new FirstView();
                    ContentControlView.DataContext = ServiceLocator.Current.GetInstance<FirstViewModel>();
                    break;

                case "SecondView":
                    ContentControlView = new SecondView();
                    ContentControlView.DataContext = ServiceLocator.Current.GetInstance<SecondViewModel>();
                    break;

                default:
                    ContentControlView = new FirstView();
                    ContentControlView.DataContext = ServiceLocator.Current.GetInstance<FirstViewModel>();
                    break;
            }
        }
    }
}
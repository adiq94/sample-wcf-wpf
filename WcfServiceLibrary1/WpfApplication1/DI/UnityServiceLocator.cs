﻿using Microsoft.Practices.ServiceLocation;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;

namespace WpfApplication1.DI
{
    public class UnityServiceLocator : IServiceLocator
    {
        private readonly IUnityContainer _unityContainer;

        public UnityServiceLocator(IUnityContainer unityContainer)
        {
            _unityContainer = unityContainer;
        }

        public IEnumerable<object> GetAllInstances(Type serviceType)
        {
            return _unityContainer.ResolveAll(serviceType);
        }

        public IEnumerable<TService> GetAllInstances<TService>()
        {
            return _unityContainer.ResolveAll<TService>();
        }

        public object GetInstance(Type serviceType)
        {
            return _unityContainer.Resolve(serviceType);
        }

        public object GetInstance(Type serviceType, string key)
        {
            return _unityContainer.Resolve(serviceType, key);
        }

        public TService GetInstance<TService>()
        {
            return _unityContainer.Resolve<TService>();
        }

        public TService GetInstance<TService>(string key)
        {
            return _unityContainer.Resolve<TService>(key);
        }

        public object GetService(Type serviceType)
        {
            throw new NotImplementedException();
        }
    }
}
﻿namespace WpfApplication1.Messaging
{
    public class SwitchViewMessage
    {
        public string ViewName { get; set; }
    }
}
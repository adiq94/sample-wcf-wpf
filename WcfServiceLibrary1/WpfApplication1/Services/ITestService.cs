﻿using System.Collections.Generic;
using System.Threading.Tasks;
using WpfApplication1.ServiceReference1;

namespace WpfApplication1.Services
{
    public interface ITestService
    {
        Task<ResponseMessage> DeleteSample(int sampleId);

        Task<ResponseMessage> EditSample(Sample sample);

        Task<string> GetData(int id);

        Task<IEnumerable<Sample>> GetSamples();

        Task<Sample> SaveSample(Sample sample);
    }
}
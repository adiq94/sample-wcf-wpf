﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WpfApplication1.ServiceReference1;

namespace WpfApplication1.Services
{
    public class TestService : ITestService
    {
        private Service1Client client;

        public async Task<ResponseMessage> DeleteSample(int sampleId)
        {
            client = new Service1Client();
            var res = await client.DeleteSampleAsync(sampleId);
            client.Close();
            return res;
        }

        public async Task<ResponseMessage> EditSample(Sample sample)
        {
            client = new Service1Client();
            var res = await client.EditSampleAsync(sample);
            client.Close();
            return res;
        }

        public async Task<string> GetData(int id)
        {
            client = new Service1Client();
            var res = await client.GetDataAsync(id);
            client.Close();
            return res;
        }

        public async Task<IEnumerable<Sample>> GetSamples()
        {
            client = new Service1Client();
            var res = await client.GetSamplesAsync();
            client.Close();
            return res;
        }

        public async Task<Sample> SaveSample(Sample sample)
        {
            client = new Service1Client();
            var res = await client.AddSampleAsync(sample);
            client.Close();
            return res;
        }
    }
}
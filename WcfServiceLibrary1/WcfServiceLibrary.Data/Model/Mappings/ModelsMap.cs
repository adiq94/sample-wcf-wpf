﻿using System.Data.Entity.ModelConfiguration;

namespace WcfServiceLibrary.Data.Model.Mappings
{
    public class SampleItemMap : EntityTypeConfiguration<SampleItem>
    {
        public SampleItemMap()
        {
            HasKey(x => x.SampleItemId);
            Property(x => x.Title).IsRequired().HasMaxLength(50);
            Property(x => x.Content).IsRequired().HasMaxLength(255);
        }
    }

    public class SampleMap : EntityTypeConfiguration<Sample>
    {
        public SampleMap()
        {
            HasKey(x => x.SampleId);
            Property(x => x.Name).IsRequired().HasMaxLength(50);
            HasMany(x => x.SampleItems).WithRequired().HasForeignKey(x => x.SampleId);
        }
    }
}
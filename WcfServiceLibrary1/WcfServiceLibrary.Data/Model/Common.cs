﻿using System.Runtime.Serialization;

namespace WcfServiceLibrary.Data.Model
{
    [DataContract]
    public class ResponseMessage
    {
        [DataMember]
        public int ErrorCode { get; set; }

        [DataMember]
        public string ErrorMessage { get; set; }
    }
}
﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace WcfServiceLibrary.Data.Model
{
    [DataContract]
    public class Sample
    {
        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public int SampleId { get; set; }

        public virtual ICollection<SampleItem> SampleItems { get; set; }
    }

    [DataContract]
    public class SampleItem
    {
        [DataMember]
        public string Content { get; set; }

        public virtual Sample Sample { get; set; }

        [DataMember]
        public int SampleId { get; set; }

        [DataMember]
        public int SampleItemId { get; set; }

        [DataMember]
        public string Title { get; set; }
    }
}
﻿using System.Data.Entity;
using WcfServiceLibrary.Data.Model;
using WcfServiceLibrary.Data.Model.Mappings;

namespace WcfServiceLibrary.Data.Database
{
    /// <summary>
    /// Neccessary level of indirection for tests with Moq
    /// </summary>
    internal interface ISampleContext
    {
        void SetDeleted(object entity);

        void SetModified(object entity);
    }

    public class SampleContext : DbContext, ISampleContext
    {
        public SampleContext() : base("DefaultConnection")
        {
            Configuration.ProxyCreationEnabled = false;
        }

        public virtual DbSet<SampleItem> SampleItems { get; set; }

        public virtual DbSet<Sample> Samples { get; set; }

        public virtual void SetDeleted(object entity)
        {
            Entry(entity).State = EntityState.Deleted;
        }

        public virtual void SetModified(object entity)
        {
            Entry(entity).State = EntityState.Modified;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new SampleMap());
            modelBuilder.Configurations.Add(new SampleItemMap());
            base.OnModelCreating(modelBuilder);
        }
    }
}
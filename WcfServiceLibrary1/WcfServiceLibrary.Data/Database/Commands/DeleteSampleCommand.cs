﻿using WcfServiceLibrary.Data.Database.Core.Base;

namespace WcfServiceLibrary.Data.Database.Commands
{
    public class DeleteSampleCommand : ICommand
    {
        public DeleteSampleCommand(int sampleId)
        {
            SampleId = sampleId;
        }

        public int SampleId { get; protected set; }
    }
}
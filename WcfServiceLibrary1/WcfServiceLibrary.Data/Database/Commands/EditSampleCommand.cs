﻿using WcfServiceLibrary.Data.Database.Core.Base;
using WcfServiceLibrary.Data.Model;

namespace WcfServiceLibrary.Data.Database.Commands
{
    public class EditSampleCommand : ICommand
    {
        public EditSampleCommand(Sample sample)
        {
            SampleId = sample.SampleId;
            Name = sample.Name;
        }

        public string Name { get; protected set; }

        public int SampleId { get; protected set; }
    }
}
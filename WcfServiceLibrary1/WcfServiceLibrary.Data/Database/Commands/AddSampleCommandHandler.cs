﻿using System;
using WcfServiceLibrary.Data.Database.Core;
using WcfServiceLibrary.Data.Database.Core.Base;
using WcfServiceLibrary.Data.Model;

namespace WcfServiceLibrary.Data.Database.Commands
{
    public class AddSampleCommandHandler : EFCommandHandlerBase<AddSampleCommand, SampleContext>, ICommandHandler<AddSampleCommand>
    {
        public AddSampleCommandHandler(SampleContext dbContext) : base(dbContext)
        {
        }

        public override void Execute(AddSampleCommand command)
        {

            Sample sample = new Sample { Name = command.Name };
            DbContext.Samples.Add(sample);
            DbContext.SaveChanges();
            command.SampleId = sample.SampleId;
        }
    }
}
﻿using WcfServiceLibrary.Data.Database.Core.Base;

namespace WcfServiceLibrary.Data.Database.Commands
{
    public class AddSampleCommand : ICommand
    {
        public AddSampleCommand(string name)
        {
            Name = name;
        }

        public string Name { get; protected set; }

        //Output
        public int SampleId { get; set; }
    }
}
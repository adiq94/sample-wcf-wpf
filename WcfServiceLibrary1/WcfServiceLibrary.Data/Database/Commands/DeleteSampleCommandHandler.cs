﻿using System;
using WcfServiceLibrary.Data.Database.Core;
using WcfServiceLibrary.Data.Database.Core.Base;
using WcfServiceLibrary.Data.Model;

namespace WcfServiceLibrary.Data.Database.Commands
{
    public class DeleteSampleCommandHandler : EFCommandHandlerBase<DeleteSampleCommand, SampleContext>, ICommandHandler<DeleteSampleCommand>
    {
        public DeleteSampleCommandHandler(SampleContext dbContext) : base(dbContext)
        {
        }

        public override void Execute(DeleteSampleCommand command)
        {

            Sample sample = new Sample() { SampleId = command.SampleId };

            DbContext.Samples.Attach(sample);

            DbContext.SetDeleted(sample);

            DbContext.SaveChanges();
        }
    }
}
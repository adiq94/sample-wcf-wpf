﻿using System;
using WcfServiceLibrary.Data.Database.Core;
using WcfServiceLibrary.Data.Database.Core.Base;
using WcfServiceLibrary.Data.Model;

namespace WcfServiceLibrary.Data.Database.Commands
{
    public class EditSampleCommandHandler : EFCommandHandlerBase<EditSampleCommand, SampleContext>, ICommandHandler<EditSampleCommand>
    {
        public EditSampleCommandHandler(SampleContext dbContext) : base(dbContext)
        {
        }

        public override void Execute(EditSampleCommand command)
        {

            Sample sample = new Sample() { SampleId = command.SampleId, Name = command.Name };

            DbContext.Samples.Attach(sample);

            DbContext.SetModified(sample);

            DbContext.SaveChanges();
        }
    }
}
﻿namespace WcfServiceLibrary.Data.Database.Core.Base
{
    public interface ICommandsFactory
    {
        void ExecuteQuery<T>(T command)
            where T : class, ICommand;
    }
}
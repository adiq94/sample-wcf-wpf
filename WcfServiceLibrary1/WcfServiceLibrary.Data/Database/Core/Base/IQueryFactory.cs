﻿namespace WcfServiceLibrary.Data.Database.Core.Base
{
    public interface IQueryFactory
    {
        T ResolveQuery<T>()
            where T : class, IQuery;
    }
}
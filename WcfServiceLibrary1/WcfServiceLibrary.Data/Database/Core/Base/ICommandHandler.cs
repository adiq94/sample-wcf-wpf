﻿using System;

namespace WcfServiceLibrary.Data.Database.Core.Base
{
    public interface ICommandHandler<in TCommand> : IDisposable
        where TCommand : ICommand
    {
        void Execute(TCommand command);
    }
}
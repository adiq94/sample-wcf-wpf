﻿using System;
using System.Data.Entity;
using WcfServiceLibrary.Data.Database.Core.Base;

namespace WcfServiceLibrary.Data.Database.Core
{
    public abstract class EFCommandHandlerBase<TCommand, TContext> : ICommandHandler<TCommand>, IDisposable
        where TCommand : Base.ICommand
        where TContext : DbContext
    {
        protected readonly TContext DbContext;

        protected EFCommandHandlerBase(TContext dbContext)
        {
            DbContext = dbContext;
        }

        public void Dispose()
        {
            DbContext.Dispose();
        }

        public abstract void Execute(TCommand command);
    }
}
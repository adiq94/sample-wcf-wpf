﻿using System.Data.Entity;
using WcfServiceLibrary.Data.Database.Core.Base;

namespace WcfServiceLibrary.Data.Database.Core
{
    public abstract class EFQueryBase<TContext> : IQuery
        where TContext : DbContext
    {
        protected readonly TContext DbContext;

        protected EFQueryBase(TContext dbContext)
        {
            DbContext = dbContext;
        }
    }
}
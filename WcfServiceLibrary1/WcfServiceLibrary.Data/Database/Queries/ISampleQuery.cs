﻿using System.Collections.Generic;
using WcfServiceLibrary.Data.Database.Core.Base;
using WcfServiceLibrary.Data.Model;

namespace WcfServiceLibrary.Data.Database.Queries
{
    public interface ISampleQuery : IQuery
    {
        IEnumerable<Sample> Execute();
    }
}
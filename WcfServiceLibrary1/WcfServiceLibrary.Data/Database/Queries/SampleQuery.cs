﻿using System.Collections.Generic;
using System.Linq;
using WcfServiceLibrary.Data.Database.Core;
using WcfServiceLibrary.Data.Model;

namespace WcfServiceLibrary.Data.Database.Queries
{
    public class SampleQuery : EFQueryBase<SampleContext>, ISampleQuery
    {
        public SampleQuery(SampleContext context) : base(context)
        {
        }

        public IEnumerable<Sample> Execute()
        {
            return DbContext.Samples.ToList();
        }
    }
}